/* BEGIN LICENSE
 * Copyright © Blue Mind SAS, 2012-2016
 *
 * This file is part of BlueMind. BlueMind is a messaging and collaborative
 * solution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU Affero General Public License as
 * published by the Free Software Foundation (version 3 of the License).
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See LICENSE.txt
 * END LICENSE
 */
package net.bluemind.vertx.common.request.impl;

import java.util.Map;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.security.cert.X509Certificate;

import com.netflix.spectator.api.Registry;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.Cookie;
import io.vertx.core.http.HttpConnection;
import io.vertx.core.http.HttpFrame;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerFileUpload;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.HttpVersion;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.http.StreamPriority;
import io.vertx.core.net.NetSocket;
import io.vertx.core.net.SocketAddress;
import io.vertx.core.streams.Pipe;
import io.vertx.core.streams.WriteStream;
import net.bluemind.metrics.registry.IdFactory;

public class WrappedRequest implements HttpServerRequest {

	private final HttpServerRequest impl;
	private final WrappedResponse response;

	private WrappedRequest(Registry registry, IdFactory idfactory, HttpServerRequest impl) {
		this.impl = impl;
		this.response = new WrappedResponse(registry, idfactory, impl.response());
	}

	public String logAttribute(String k) {
		return response.getLogAttribute(k);
	}

	public void putLogAttribute(String k, String v) {
		response.putLogAttribute(k, v);
	}

	public static HttpServerRequest create(Registry registry, IdFactory idfactory, HttpServerRequest req) {
		return new WrappedRequest(registry, idfactory, req);
	}

	public HttpServerRequest exceptionHandler(Handler<Throwable> handler) {
		impl.exceptionHandler(handler);
		return this;
	}

	public HttpServerRequest pause() {
		impl.pause();
		return this;
	}

	public HttpServerRequest endHandler(Handler<Void> endHandler) {
		impl.endHandler(endHandler);
		return this;
	}

	public HttpServerRequest resume() {
		impl.resume();
		return this;
	}

	public HttpVersion version() {
		return impl.version();
	}

	public String uri() {
		return impl.uri();
	}

	public String path() {
		return impl.path();
	}

	public String query() {
		return impl.query();
	}

	public HttpServerResponse response() {
		return response;
	}

	public MultiMap headers() {
		return impl.headers();
	}

	public MultiMap params() {
		return impl.params();
	}

	public SocketAddress remoteAddress() {
		return impl.remoteAddress();
	}

	public X509Certificate[] peerCertificateChain() throws SSLPeerUnverifiedException {
		return impl.peerCertificateChain();
	}

	public HttpServerRequest bodyHandler(Handler<Buffer> bodyHandler) {
		impl.bodyHandler(bodyHandler);
		return this;
	}

	public NetSocket netSocket() {
		return impl.netSocket();
	}

	public HttpServerRequest uploadHandler(Handler<HttpServerFileUpload> uploadHandler) {
		impl.uploadHandler(uploadHandler);
		return this;
	}

	public MultiMap formAttributes() {
		return impl.formAttributes();
	}

	public HttpServerRequest handler(Handler<Buffer> handler) {
		impl.handler(handler);
		return this;
	}

	public HttpServerRequest fetch(long amount) {
		impl.fetch(amount);
		return this;
	}

	public HttpMethod method() {
		return impl.method();
	}

	public String rawMethod() {
		return impl.rawMethod();
	}

	public boolean isSSL() {
		return impl.isSSL();
	}

	public String scheme() {
		return impl.scheme();
	}

	public String host() {
		return impl.host();
	}

	public long bytesRead() {
		return impl.bytesRead();
	}

	public String getHeader(String headerName) {
		return impl.getHeader(headerName);
	}

	public String getHeader(CharSequence headerName) {
		return impl.getHeader(headerName);
	}

	public Pipe<Buffer> pipe() {
		return impl.pipe();
	}

	public String getParam(String paramName) {
		return impl.getParam(paramName);
	}

	public void pipeTo(WriteStream<Buffer> dst) {
		impl.pipeTo(dst);
	}

	public void pipeTo(WriteStream<Buffer> dst, Handler<AsyncResult<Void>> handler) {
		impl.pipeTo(dst, handler);
	}

	public SocketAddress localAddress() {
		return impl.localAddress();
	}

	public SSLSession sslSession() {
		return impl.sslSession();
	}

	public String absoluteURI() {
		return impl.absoluteURI();
	}

	public HttpServerRequest setExpectMultipart(boolean expect) {
		impl.setExpectMultipart(expect);
		return this;
	}

	public boolean isExpectMultipart() {
		return impl.isExpectMultipart();
	}

	public String getFormAttribute(String attributeName) {
		return impl.getFormAttribute(attributeName);
	}

	public ServerWebSocket upgrade() {
		return impl.upgrade();
	}

	public boolean isEnded() {
		return impl.isEnded();
	}

	public HttpServerRequest customFrameHandler(Handler<HttpFrame> handler) {
		impl.customFrameHandler(handler);
		return this;
	}

	public HttpConnection connection() {
		return impl.connection();
	}

	public StreamPriority streamPriority() {
		return impl.streamPriority();
	}

	public HttpServerRequest streamPriorityHandler(Handler<StreamPriority> handler) {
		impl.streamPriorityHandler(handler);
		return this;
	}

	public Cookie getCookie(String name) {
		return impl.getCookie(name);
	}

	public int cookieCount() {
		return impl.cookieCount();
	}

	public Map<String, Cookie> cookieMap() {
		return impl.cookieMap();
	}

}
