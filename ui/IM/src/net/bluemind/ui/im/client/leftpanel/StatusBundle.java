package net.bluemind.ui.im.client.leftpanel;

import com.google.gwt.resources.client.ClientBundle;

public interface StatusBundle extends ClientBundle {
	@Source("Status.css")
	StatusStyle getStyle();

}
