/* BEGIN LICENSE
 * Copyright © Blue Mind SAS, 2012-2016
 *
 * This file is part of BlueMind. BlueMind is a messaging and collaborative
 * solution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU Affero General Public License as
 * published by the Free Software Foundation (version 3 of the License).
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See LICENSE.txt
 * END LICENSE
 */
package net.bluemind.ui.im.client;

public final class Screens {

	public static final String LEFT_PANEL = "leftPanel";
	public static final String CONVERSATIONS = "conversations";
	public static final String CREATE_CONVERSATION = "createConversation";
	public static final String ADD_TO_FAVORITES = "addToFavorites";
	public static final String REMOVE_FROM_FAVORITES = "removeFromFavorites";
	public static final String INVITE_TO_CHATROOM = "inviteToChatroom";
	public static final String SEND_HISTORY = "sendHistory";
	public static final String SUBSCRIPTION_REQUEST = "subscriptionRequest";
	public static final String NEW_INVITATION = "newInvitation";
	public static final String OVERLAY = "overlay";

}
