import "@testing-library/jest-dom";
import registerRequireContextHook from "babel-plugin-require-context-hook/register";

registerRequireContextHook();
