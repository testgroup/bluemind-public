import merge from "lodash.merge";
import Vuex from "vuex";
import { createLocalVue, mount } from "@vue/test-utils";

import AlertStore from "@bluemind/alert.store";
import inject from "@bluemind/inject";

import MailAppStore from "../src/store";
import { messageKey as generateKey } from "../src/model/message";

const localVue = createLocalVue();
localVue.use(Vuex);

const folderUid = "folder:uid";
export const conversationKey = generateKey(1, folderUid);
export const messageKey = generateKey(1, folderUid);
const userId = "6793466E-F5D4-490F-97BF-DF09D3327BF4";

inject.register({
    provide: "UserSession",
    use: { userId }
});

inject.register({
    provide: "i18n",
    use: {
        t: () => {},
        tc: () => {}
    }
});

const mailbox = {
    key: "MY_MAILBOX",
    type: "users",
    owner: userId,
    writable: true
};

const fakedSessionStore = {
    namespaced: true,
    state: { settings: { remote: { mail_thread: "false" } } }
};

export function createStore() {
    const store = new Vuex.Store();
    store.registerModule("alert", AlertStore);
    store.registerModule("mail", MailAppStore);
    store.registerModule("session", fakedSessionStore);
    store.commit("mail/SET_MAX_MESSAGE_SIZE", 10);
    store.commit("mail/ADD_MAILBOXES", [mailbox]);
    store.commit("mail/SET_MAILBOX_FOLDERS", {
        folders: [
            { key: folderUid, mailboxRef: { key: "MY_MAILBOX" }, writable: true, path: "my/folder" },
            { key: "TraskKey", imapName: "Trash", path: "Trash", mailboxRef: { key: "MY_MAILBOX" }, writable: true },
            { key: "SentKey", imapName: "Sent", path: "Sent", mailboxRef: { key: "MY_MAILBOX" }, writable: true }
        ],
        mailbox
    });
    const conversations = [
        {
            key: conversationKey,
            flags: [],
            folderRef: { key: folderUid, uid: folderUid },
            messages: [messageKey],
            date: new Date(123456)
        }
    ];
    const messages = [
        { key: messageKey, folderRef: { key: folderUid, uid: folderUid }, conversationRef: { key: conversationKey } }
    ];
    store.commit("mail/SET_CONVERSATION_LIST", { conversations, messages });
    store.commit("mail/SET_CURRENT_CONVERSATION", conversations[0]);
    store.commit("mail/SET_ACTIVE_FOLDER", { key: folderUid });

    return store;
}

export function createWrapper(component, overrides, propsData = {}) {
    const defaultMountingOptions = {
        localVue,
        store: createStore(),
        propsData: propsData,
        mocks: {
            $t: () => {},
            $tc: () => {}
        }
    };
    const mergedMountingOptions = merge(defaultMountingOptions, overrides);
    return mount(component, mergedMountingOptions);
}
