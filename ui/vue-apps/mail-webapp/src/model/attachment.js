export function create(part, status) {
    const progress = status === AttachmentStatus.NOT_LOADED ? { loaded: 0, total: 100 } : { loaded: 100, total: 100 };
    return { ...part, dispositionType: "ATTACHMENT", headers: getAttachmentHeaders(part), progress, status };
}

export const AttachmentStatus = {
    ONLY_LOCAL: "ONLY_LOCAL",
    NOT_LOADED: "NOT-LOADED",
    UPLOADED: "UPLOADED",
    ERROR: "ERROR"
};

export function getAttachmentHeaders({ fileName, size }) {
    return [
        {
            name: "Content-Disposition",
            values: ["attachment;filename=" + fileName + ";size=" + size]
        },
        {
            name: "Content-Transfer-Encoding",
            values: ["base64"]
        }
    ];
}

/**
 * @return true if we consider the given part should be attached,
 *         i.e.: not shown in the message body, false otherwise
 */
export function isAttachment(part) {
    return part.dispositionType && part.dispositionType === "ATTACHMENT";
}
