export const DEFAULT_FOLDER_NAMES = {
    INBOX: "INBOX",
    SENT: "Sent",
    DRAFTS: "Drafts",
    TRASH: "Trash",
    JUNK: "Junk",
    OUTBOX: "Outbox"
};

export const DEFAULT_FOLDERS = Object.values(DEFAULT_FOLDER_NAMES);
