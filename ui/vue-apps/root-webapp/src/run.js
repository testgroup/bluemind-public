import Vue from "vue";
import Vue2TouchEvents from "vue2-touch-events";
import VueI18n from "vue-i18n";

import { default as AlertStore, DefaultAlert } from "@bluemind/alert.store";
import { generateDateTimeFormats, InheritTranslationsMixin } from "@bluemind/i18n";
import injector from "@bluemind/inject";
import router from "@bluemind/router";
import { initSentry } from "@bluemind/sentry";
import store from "@bluemind/store";
import { BmModalPlugin } from "@bluemind/styleguide";
import UUIDGenerator from "@bluemind/uuid";
import VueBus from "@bluemind/vue-bus";
import { extend } from "@bluemind/vuex-router";
import VueSockjsPlugin from "@bluemind/vue-sockjs";

import registerDependencies from "./registerDependencies";
import PreferencesStore from "./preferencesStore";
import RootAppStore from "./rootAppStore";
import SessionStore from "./sessionStore";
import MainApp from "./components/MainApp";
import NotificationManager from "./NotificationManager";

initWebApp();
initSentry(Vue);

async function initWebApp() {
    registerUserSession();
    const userSession = injector.getProvider("UserSession").get();
    registerDependencies(userSession);
    initStore();
    setVuePlugins(userSession);
    if (userSession.userId) {
        await store.dispatch("session/FETCH_ALL_SETTINGS"); // needed to initialize i18n
    }
    const i18n = initI18N(userSession);
    Vue.component("DefaultAlert", DefaultAlert);
    adaptLegacyNotificationSystem();
    new Vue({
        el: "#app",
        i18n,
        render: h => h(MainApp),
        router,
        store
    });
    if (userSession.userId) {
        new NotificationManager().setNotificationWhenReceivingMail(userSession);
    }
}

function adaptLegacyNotificationSystem() {
    Vue.component("DisplayLegacyNotif", {
        name: "DisplayLegacyNotif",
        props: {
            alert: {
                type: Object,
                default: () => ({})
            }
        },
        template: "<span>{{alert.payload}}</span>"
    });
    document.addEventListener("ui-notification", displayLegacyNotification);
}

function displayLegacyNotification(event) {
    const type = event.detail.type !== "error" ? "SUCCESS" : "ERROR";
    store.dispatch("alert/" + type, {
        alert: { uid: UUIDGenerator.generate(), name: "legacy notif", payload: event.detail.message },
        options: { renderer: "DisplayLegacyNotif" }
    });
}

function setVuePlugins(userSession) {
    Vue.use(VueI18n);
    Vue.use(VueBus, store);
    if (userSession.userId) {
        Vue.use(VueSockjsPlugin, VueBus);
    }
    Vue.use(Vue2TouchEvents, { disableClick: true });
    Vue.use(BmModalPlugin);
}

function registerUserSession() {
    injector.register({
        provide: "UserSession",
        use: window.bmcSessionInfos
    });
}

function initStore() {
    extend(router, store);
    store.registerModule("alert", AlertStore);
    store.registerModule("root-app", RootAppStore);
    store.registerModule("session", SessionStore);
    store.registerModule("preferences", PreferencesStore);
}

function initI18N() {
    // lang can be any of AvailableLanguages
    const lang = store.state.session.settings.remote.lang;

    Vue.mixin(InheritTranslationsMixin);

    let fallbackLang = "en";
    const navigatorLang = navigator.language;
    if (navigatorLang) {
        fallbackLang = navigator.language.split("-")[0];
    }
    const i18n = new VueI18n({
        locale: lang,
        fallbackLocale: fallbackLang,
        dateTimeFormats: generateDateTimeFormats(store.state.session.settings.remote.timeformat)
    });

    injector.register({
        provide: "i18n",
        use: i18n
    });

    return i18n;
}
