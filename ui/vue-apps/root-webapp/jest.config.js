const baseConfig = require("@bluemind/conf/jest.config.base");

module.exports = {
    name: "root-webapp",
    displayName: "Root WebApp",
    ...baseConfig
};
