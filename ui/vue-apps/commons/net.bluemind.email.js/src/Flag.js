export default {
    SEEN: "\\Seen",
    ANSWERED: "\\Answered",
    FLAGGED: "\\Flagged",
    FORWARDED: "$Forwarded"
};
