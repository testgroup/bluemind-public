export default {
    HAS_MAIL_WEBAPP: "hasMailWebapp",
    HAS_WEBMAIL: "hasWebmail",
    HAS_MAIL: "hasMail",
    HAS_CALENDAR: "hasCalendar",
    HAS_CTI: "hasCTI",
    HAS_IM: "hasIM",
    HAS_TBIRD: "hasTbird",

    SELF_CHANGE_MAIL_IDENTITIES: "selfChangeMailIdentities",
    SELF_CHANGE_MAILBOX_FILTER: "selfChangeMailboxFilter",
    SELF_CHANGE_PASSWORD: "selfChangePassword",
    SELF_CHANGE_SETTINGS: "selfChangeSettings",
    SELF_MANAGE_DEVICE: "selfManageDevice",
    SELF_MANAGE_EXTERNAL_ACCOUNT: "selfManageExternalAccount",
    SELF_MANAGE_VCARD: "selfManageVCard",

    CAN_CREATE_EXTERNAL_IDENTITY: "canCreateExternalIdentity",
    MAIL_FORWARDING: "mailForwarding",
    MANAGE_MAILBOX_IDENTITIES: "manageMailboxIdentities",
    READ_DOMAIN_FILTERS: "readDomainFilters"
};
